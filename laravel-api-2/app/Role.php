<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Role extends Model
{
    protected $fillable = ['id','name'];
    protected $primaryKey = 'id';
    protected $keyType = 'string';
    public $incrementing = false;

    protected static function boot(){
        parent::boot();
        static::creating( function($model){
            if(empty($model->id)){
                $model->id = Str::uuid();
            }
        });
    }

    public function user(){
        return $this->belongsTo('App\User');
        return $this->hasMany('App\User');
    }
}
