<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\Comment;
use Illuminate\Support\Facades\Validator;

class PostController extends Controller
{
    public function __construct(){
        return $this->middleware('auth:api')->only(['store', 'update', 'delete']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $post = Post::latest()->get();
        return response()->json([
            'success' => true,
            'message' => 'List Data Post',
            'data' => $post
        ], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'post' => 'required',
            'description' =>'required'
        ]);
        if ($validator->fails()){
            return response()->json($validator->errors(), 400);
        }
        $post = Post::create([
            'post' => $request->post,
            'description' => $request->description
        ]);
        if ($post){
            return response()->json([
                'success' => true,
                'message' => 'Post Created',
                'data' => $post
            ], 201);
        }
        return response()->json([
            'success' => false,
            'message' => 'Post Failed to Save'
        ], 409);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::findOrFail($id);
        return response()->json([
            'success' => true,
            'message' => 'Detail Data Post',
            'data' => $post
        ], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(),[
            'post' => 'required',
            'description' => 'required'
        ]);
        if ($validator->fails()){
            return response()->json($validator->errors(), 400);
        }
        $post = Post::find($id);
        if ($post){
            $user = auth()->user();
            if($post->user_id != $user->id){
                return response()->json([
                    'success' => false,
                    'message' => 'Anda tidak berhak merubah postingan'
                ],403);
            }
            $post->update([
                'post' => $request->post,
                'description' => $request->description
            ]);
            return response()->json([
                'success' => true,
                'message' => 'Post ' .$post->post. ' updated',
                'data' =>$post
            ],200);
        }

        return response()->json([
            'success'=>false,
            'message'=> 'Post Not Found'
        ], 404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::find($id);
        $comment = Comment::where('post_id', '=', $id);
        if ($post){
            $user = auth()->user();
            if($post->user_id != $user->id){
                return response()->json([
                    'success' => false,
                    'message' => 'Anda tidak berhak menghapus postingan'
                ],403);
            }
            $comment->delete();
            $post->delete();
            return response()->json([
                'success' => true,
                'message' => 'Post Deleted'
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Post Not Found'
        ], 400);
    }
}
