<?php

namespace App\Http\Controllers;

use App\Comment;
use Illuminate\Http\Request;
use App\Events\CommentStoredEvent;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class CommentController extends Controller
{
    public function __construct(){
        return $this->middleware('auth:api')->except(['index', 'show']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $comment = Comment::latest()->get();
        return response()->json([
            'success' => true,
            'message' => 'List Data Comment',
            'data' => $comment
        ], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'content' => 'required',
            'post_id' =>'required'
        ]);
        if ($validator->fails()){
            return response()->json($validator->errors(), 400);
        }
        $comment = Comment::create([
            'content' => $request->content,
            'post_id' => $request->post_id
        ]);


        event(new CommentStoredEvent($comment));
        // Mail::to($comment->post->user->email)->send(new PostAuthorMail($comment));
        // Mail::to($comment->user->email)->send(new CommentAuthorMail($comment));
        if ($comment){
            return response()->json([
                'success' => true,
                'message' => 'Comment Created',
                'data' => $comment
            ], 201);
        }
        return response()->json([
            'success' => false,
            'message' => 'Comment Failed to Save'
        ], 409);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $comment = Comment::findOrFail($id);
        return response()->json([
            'success' => true,
            'message' => 'Detail Data Comment',
            'data' => $post
        ], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(),[
            'content' => 'required',
            'post_id' => 'required'
        ]);
        if ($validator->fails()){
            return response()->json($validator->errors(), 400);
        }
        $comment = Comment::find($id);
        if ($comment){
            $user = auth()->user();
            if($comment->user_id != $user->id){
                return response()->json([
                    'success' => false,
                    'message' => 'Anda tidak berhak merubah komentar'
                ],403);
            }
            $comment->update([
                'content' => $request->content,
                'post_id' => $request->post_id
            ]);
            return response()->json([
                'success' => true,
                'message' => 'Comment Updated',
                'data' =>$comment
            ],200);
        }

        return response()->json([
            'success'=>false,
            'message'=> 'Comment Not Found'
        ], 404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $comment = Comment::find($id);
        if ($comment){
            $user = auth()->user();
            if($comment->user_id != $user->id){
                return response()->json([
                    'success' => false,
                    'message' => 'Anda tidak berhak menghapus komentar'
                ],403);
            }
            $comment->delete();
            return response()->json([
                'success' => true,
                'message' => 'Comment Deleted'
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Comment Not Found'
        ], 400);
    }
}
