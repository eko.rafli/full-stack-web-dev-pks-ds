<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Role;
use Illuminate\Support\Facades\Validator;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $role = Role::latest()->get();
        return response()->json([
            'success' => true,
            'message' => 'List Data Role',
            'data' => $role
        ], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'name' => 'required'
        ]);
        if ($validator->fails()){
            return response()->json($validator->errors(), 400);
        }
        $role = Role::create([
            'name' => $request->name,
        ]);
        if ($role){
            return response()->json([
                'success' => true,
                'message' => 'Role Created',
                'data' => $role
            ], 201);
        }
        return response()->json([
            'success' => false,
            'message' => 'Role Failed to Save'
        ], 409);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $role = Role::findOrFail($id);
        return response()->json([
            'success' => true,
            'message' => 'Detail Data Role',
            'data' => $role
        ], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Role $role)
    {
        $validator = Validator::make($request->all(),[
            'name' => 'required',
        ]);
        if ($validator->fails()){
            return response()->json($validator->errors(), 400);
        }
        $role = Role::findOrFail($role->id);
        if ($role){
            $role->update([
                'name' => $request->name,
            ]);
            return response()->json([
                'success' => true,
                'message' => 'Role Updated',
                'data' =>$role
            ],200);
        }

        return response()->json([
            'success'=>false,
            'message'=> 'Role Not Found'
        ], 404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($name)
    {
        $role = Role::findOrFail($name);
        if ($role){
            $role->delete();
            return response()->json([
                'success' => true,
                'message' => 'Role Deleted'
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Role Not Found'
        ], 400);
    }
}
