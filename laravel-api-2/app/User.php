<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Support\Str;
use App\Role;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;
    protected $fillable=['id', 'username', 'email', 'role_id', 'email_verified_at', 'password'];
    protected $primaryKey = 'id';
    protected $keyType = 'string';
    public $incrementing = false;

    protected static function boot(){
        parent::boot();
        static::creating( function($model){
            if(empty($model->id)){
                $model->id = Str::uuid();
            }
            $model->role_id = Role::where('name', 'author')->first()->id;
        });
    }

    public function role(){
        return $this->belongsTo('App\Role');
        return $this->hasOne('App\Role');
    }

    public function otp_code(){
        return $this->hasOne('App\OtpCode');
    }
    public function post(){
        return $this->hasMany('App\Post');
    }
    public function comment(){
        return $this->hasMany('App\Comment');
    }

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
}
