var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000},
    {name: 'komik', timeSpent: 1000}
]
// JAWABAN SOAL 2
readBooksPromise(10000, books[0])
    .then(s1 => {readBooksPromise(s1, books[1])
        .then(s2 => {readBooksPromise(s2, books[2])
            .then(s3 => {readBooksPromise(s3, books[3])
                .then(s4 => console.log(`sisa waktu membaca ${s4}`))
                .catch(error => console.log(error))})
            .catch(error => console.log(error))})
        .catch(error => console.log(error))})
    .catch(error => console.log(error))