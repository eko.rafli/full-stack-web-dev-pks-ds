<?php

trait Hewan {
    public $nama;
    public $darah=50;
    public $jumlahKaki;
    public $keahlian;
    public function atraksi(){
        echo $this->nama. " sedang " . $this->keahlian;
    }
}

trait Fight {
    public $attackPower;
    public $defencePower;
    public function serang($korban, $darah, $defKorban){
        echo $this->nama. " sedang menyerang " .$korban;
        $this->diserang($korban, $darah, $defKorban);
    }
    public function diserang($korban, $darah, $defKorban){
        echo "<br>". $korban. " sedang diserang " . $this->nama;
        $setdarah = $darah - ($this->attackPower / $defKorban);
        echo "<br>";
        echo "Darah " .$korban. " berkurang menjadi " .$setdarah;
    }
}

class Elang {
    use Hewan, Fight;
    public function __construct($nama, $jumlahKaki, $keahlian, $attackPower, $defencePower, $darah){
        $this->nama = $nama;
        $this->jumlahKaki = $jumlahKaki;
        $this->keahlian = $keahlian;
        $this->attackPower = $attackPower;
        $this->defencePower = $defencePower;
        $this->darah = $darah;
    }
    public function getInfoHewan(){
        echo "Nama Hewan : " .$this->nama. "<br>Jumlah Kaki : " .$this->jumlahKaki. "<br>Keahlian : " .$this->keahlian. "<br> Attack Power : " .$this->attackPower. "<br> Defence Power : " .$this->defencePower. "<br> Darah : " .$this->darah;
    }

}

class Harimau {
    use Hewan, Fight;
    public function __construct($nama, $jumlahKaki, $keahlian, $attackPower, $defencePower, $darah){
        $this->nama = $nama;
        $this->jumlahKaki = $jumlahKaki;
        $this->keahlian = $keahlian;
        $this->attackPower = $attackPower;
        $this->defencePower = $defencePower;
        $this->darah = $darah;
    }
    public function getInfoHewan(){
        echo "Nama Hewan : " .$this->nama. "<br>Jumlah Kaki : " .$this->jumlahKaki. "<br>Keahlian : " .$this->keahlian. "<br> Attack Power : " .$this->attackPower. "<br> Defence Power : " .$this->defencePower. "<br> Darah : " .$this->darah;
    }
}

$elang = new Elang("Eagle", 2, "terbang tinggi", 10, 5, 50);
$harimau = new Harimau("Tiger", 4, "lari cepat", 7, 8, 50);
$elang->atraksi();
echo "<br>";
$harimau->atraksi();
echo "<br>";
echo $elang->getInfoHewan();
echo "<br>";
$elang->serang($harimau->nama, $harimau->darah, $harimau->defencePower);
echo "<br>";
?>