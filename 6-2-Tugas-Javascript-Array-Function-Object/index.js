
// Soal 1
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"]
daftarHewan.sort()
for (i=0; i<daftarHewan.length; i++){
    console.log(daftarHewan[i]);
}

// Soal 2
function introduce(isian){
    return "Nama saya " + isian.name + ", umur saya " + isian.age + " tahun, alamat saya di " + isian.address + ", dan saya punya hobby " + isian.hobby;
}
var data = {name : "John", age : "30", address : "Jalan Pelesiran", hobby : "Gaming"}
var perkenalan = introduce(data)
console.log(perkenalan)

// Soal 3
function hitung_huruf_vokal(cth){
    huruf = cth.toLowerCase()
    huruf.split('')
    var jmlhuruf = 0
    for (i=0; i<huruf.length; i++){
        if(huruf[i] == 'a' || huruf[i] == 'i' || huruf[i] == 'u' || huruf[i] == 'e' || huruf[i] == 'o' ) {
            jmlhuruf++;
        }
    }
    return jmlhuruf
}
var hitung_1 = hitung_huruf_vokal("Muhammad")
var hitung_2 = hitung_huruf_vokal("Iqbal")
console.log(hitung_1, hitung_2)

// Soal 4
function hitung(angka){
    hasil = (angka*2) - 2
    return hasil
}
console.log(hitung(0))
console.log(hitung(1))
console.log(hitung(2))
console.log(hitung(3))
console.log(hitung(5))